export default {
  position: 0,
  viewed_games: [],
  games: [
    {
      authors: ["David Mulvey"],
      players: 1,
      description: "From the classic Asteroids comes this new rendition built with processing. Dont let the asteroids pass or you lose points, dont let them hit you or you lose lives. Good Luck!",
      display_name: "Asteroids",
      art: 'https://i.imgur.com/vZDyikH.png',
      repo: "asteroids",
      active: false
    },
    {
      authors: ["Kevin Brodsack"],
      players: 1,
      display_name: "Random Stuff Generator",
      repo: "rsg",
      active: false
    },
    {
      authors: ["Thomas Ward"],
      players: 2,
      description: "Fight one-on-one in a game where two castles- red and blue- duke it out in a full on brawl of the century! Gather resources to get the upperhand on your opponent",
      display_name: "Pevanshire",
      repo: "pevanshire",
      active: false
    },
    {
      authors: ["Sol Fritz", "David Mulvey"],
      players: 1,
      description: "Processing Remake of classic Donkey Kong!",
      display_name: "Donkey Kong",
      repo: "donkeykong",
      active: false
    },
    {
      authors: ["Your Name Here"],
      players: 1,
      display_name: "Placeholder A",
      repo: "donkeykong",
      active: false
    },
    {
      authors: ["Your Name Here"],
      players: 1,
      display_name: "Placeholder B",
      repo: "donkeykong",
      active: false
    },
    {
      authors: ["Your Name Here"],
      players: 1,
      display_name: "Placeholder C",
      repo: "donkeykong",
      active: false
    }
  ]
};
